import sys
from collections import deque

from PyQt5.QtCore import QObject, QThread, QUrl, Qt, pyqtSignal
from PyQt5.QtGui import QPixmap, QIcon, QCursor, QDesktopServices
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QTreeWidgetItem, QErrorMessage

import ui.about
import ui.form
import ui.patent
import ui.patents_list

import lib.LibParse as LibParse
import lib.LibParseV2 as LibParseV2
import lib.LibExcel as excel
    

class AboutWindow(QWidget, ui.about.Ui_Dialog):
    def __init__(self, parent=None):
        super(AboutWindow, self).__init__(parent)
        self.setupUi(self)
        self.label_4.setPixmap(QPixmap('img/icon.svg'))
        self.label_4.setScaledContents(True)
        self.setWindowIcon(QIcon('img/icon.svg'))


class PatentWindow(QWidget, ui.patent.Ui_Dialog):
    def __init__(self, parent=None, patent_info={}, all_patents=[]):
        super(PatentWindow, self).__init__(parent)
        self.setupUi(self)
        self.setWindowIcon(QIcon('img/icon.svg'))
        self.__patent_info = patent_info
        self.__patent_list_windows = []
        self.__all_patents = all_patents
        self.textBrowser.setText(f'{self.__patent_info["ID"]} - {self.__patent_info["info"]["title"]}')
        self.setWindowTitle(f'{self.__patent_info["ID"]} - {self.__patent_info["info"]["title"]}')
        self.draw_list()

        self.pushButton.clicked.connect(self.close)
        self.treeWidget.doubleClicked.connect(self.goto_link)

    def draw_list(self):
        self.treeWidget.topLevelItem(0).setText(1, ', '.join(self.__patent_info["info"]["assignee_name"]))
        self.treeWidget.topLevelItem(1).setText(1, ', '.join(self.__patent_info['info']['inventor_name']))
        self.treeWidget.topLevelItem(2).setText(1, f'{self.__patent_info["info"]["application_date"]}')
        self.treeWidget.topLevelItem(3).setText(1, f'https://patents.google.com/patent/{self.__patent_info["ID"]}')
        self.treeWidget.topLevelItem(4).setText(1, self.__patent_info['info']['pdf_url'])
        self.treeWidget.topLevelItem(5).setText(1, ', '.join(self.__patent_info['applications']) if
        len(self.__patent_info['applications']) > 0 else 'Отсутствуют')
        self.treeWidget.topLevelItem(6).setText(1, ', '.join(self.__patent_info['children']) if
        len(self.__patent_info['children']) > 0 else 'Отсутствуют')
        self.treeWidget.topLevelItem(7).setText(1, ', '.join(self.__patent_info['parents']) if
        len(self.__patent_info['parents']) > 0 else 'Отсутствуют')

        self.treeWidget.resizeColumnToContents(0)
        self.treeWidget.show()

    def goto_link(self, val):
        try:
            if val.row() == 3 or val.row() == 4:
                if val.data() != 'Отсутствует':
                    QDesktopServices.openUrl(QUrl(val.data()))
            elif (val.row() == 5 or val.row() == 6 or val.row() == 7) and 'Отсутствуют' not in val.data():
                self.show_patents_list_window(val)
        except Exception as err:
            print(err)

    def show_patents_list_window(self, val):
        try:
            selected = val.data().split(', ')
            option = ''
            if val.row() == 5:
                option = 'application'
            elif val.row() == 6:
                option = 'children'
            elif val.row() == 7:
                option = 'parents'

            self.__patent_list_windows.append(PatentsListWindow(None, self.__all_patents, option, val.data(), selected))
            self.__patent_list_windows[-1].show()
        except Exception as err:
            print(err)


class PatentsListWindow(QWidget, ui.patents_list.Ui_Dialog):
    def __init__(self, parent=None, patents_list=None, option='', parent_id='', selected=''):
        super(PatentsListWindow, self).__init__(parent)
        if patents_list is None:
            patents_list = []
        self.setupUi(self)
        self.setWindowIcon(QIcon('img/icon.svg'))
        self.__patents_list = patents_list
        self.__parent_id = parent_id
        self.__selected = selected
        self.__patent_windows = []
        self.draw_list()
        if option == 'applications':
            self.setWindowTitle(f'Применения {self.__parent_id}')
        elif option == 'children':
            self.setWindowTitle(f'Цитирования {self.__parent_id}')
        elif option == 'parents':
            self.setWindowTitle(f'{self.__parent_id} - кого цитировал')

        self.treeWidget.doubleClicked.connect(self.show_patent_window)
        self.pushButton.clicked.connect(self.close)

    def draw_list(self):
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        patents = [QTreeWidgetItem([f'{item["ID"]} - {item["info"]["title"]}']) for item in self.__patents_list
                   if item['ID'] in self.__selected]

        to_process = ' '.join([item['ID'] for item in self.__patents_list if item['ID'] in self.__selected])
        to_process = [item for item in self.__selected if item not in to_process]

        processed = [LibParse.PatentsParser.parse_patent_page(self, pat) for pat in to_process]
        del to_process

        [self.__patents_list.append(item) for item in processed]
        [patents.append(QTreeWidgetItem([f'{item["ID"]} - {item["info"]["title"]}'])) for item in processed]

        self.treeWidget.setColumnCount(1)
        self.treeWidget.addTopLevelItems(patents)

        self.treeWidget.show()
        QApplication.restoreOverrideCursor()

    def show_patent_window(self, val):
        patent = [item for item in self.__patents_list if item['ID'] in val.data()]

        self.__patent_windows.append(PatentWindow(patent_info=patent[-1]))
        self.__patent_windows[-1].show()


class MainWindow(QMainWindow, ui.form.Ui_MainWindow):
    def __init__(self, parent=None):
        # ---- Window init ----
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.setWindowIcon(QIcon('img/icon.svg'))
        self.setWindowTitle('Mortarboard')

        # ---- Global vars ----
        self.__dialog_about = None
        self.__patent_windows = []
        self.__thread = QThread()
        
        # ---- Class initializations
        self.__parser = LibParseV2.Parser()
        self.__tree = LibParseV2.PatentTree()

        # ---- Handlers ----
        self.searchButton.clicked.connect(self.make_thread(method='do_search'))
        self.pushButton.clicked.connect(self.make_thread(method='show_about_window'))
        self.pushButton_2.clicked.connect(self.do_excel)
        self.treeWidget.doubleClicked.connect(self.show_patent_window)
        
    class Worker(QObject):
        finished = pyqtSignal()
    
        def run(self, instance=None, method=None):
            getattr(instance, method)
            self.finished.emit()
            
    def make_thread(self, method=None):
        worker = self.Worker
        worker.moveToThread(self, self.__thread)
        self.__thread.started.connect(worker.run(worker, instance=self, method=method))
        worker.finished.connect(self.__thread.quit)
        worker.finished.connect(worker.deleteLater)
        self.__thread.finished.connect(self.__thread.deleteLater)
        self.__thread.start()
        self.searchButton.setEnabled(False)
        self.__thread.finished.connect(lambda: self.searchButton.setEnabled(True))

    def do_error(self, error):
        QApplication.restoreOverrideCursor()
        error_dialog = QErrorMessage()
        error_dialog.setWindowTitle('Ошибка')
        if error == 'no_req':
            error_dialog.showMessage('Введите запрос!')
        elif error == 'no_patents_excel':
            error_dialog.showMessage('Отсутсвует информация о патентах, совершите поиск!')
        elif error == 'timeout':
            error_dialog.showMessage('Таймаут')
        self.__patent_windows.append(error_dialog)

    def do_excel(self):
        if len(self.__parsed) > 0:
            QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
            excel.SaveContents(self.__parsed)
            QApplication.restoreOverrideCursor()
        else:
            self.do_error(error='no_patents_excel')

    def show_about_window(self):
        self.__dialog_about = AboutWindow()
        self.__dialog_about.show()

    def show_patent_window(self, val):
        patent = [item for item in self.__parsed if item['ID'] in val.data()]

        self.__patent_windows.append(PatentWindow(patent_info=patent[-1]))
        self.__patent_windows[-1].show()

    def do_search(self):
        self.progressBar.setValue(0)
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        request = self.lineEdit.text()
        pages = self.spinBox.value()
        quantity = self.spinBox_2.value()
        id_queue = deque()
        
        if len(request) == 0:
            self.do_error(error='no_req')
        else:
            try:
                for i in range(pages):
                    id_queue.extend(self.__parser.parse_search(request=request.split(), page_num=i))
                
                try:
                    for i in range(quantity):
                        current = id_queue.pop()
                        patent = self.__tree.restore_patent(current)
                        if patent is not None:
                            if 'citations' in patent.info:
                                id_queue.extendleft(patent.info['citations'])
                        else:
                            info = self.__parser.parse_patent(patent_id=current)
                            if 'citations' in info:
                                id_queue.extendleft(info['citations'])
                            patent = self.__tree.create_patent(patent_id=current, info=info)
                        self.progressBar.setValue((i / quantity) * 100)
                except:
                    pass   
                    
                self.__tree.cache_patents()
                self.progressBar.setValue(100)
                self.draw_tree()
                QApplication.restoreOverrideCursor()
                
            except Exception as e:
                print(e)
                self.do_error(error='timeout')

    def draw_tree(self):
        if self.treeWidget.topLevelItemCount() != 0:
            self.treeWidget.clear()

        def add_children(rt, array):
            if len(array) > 0 and len(rt) == 2:
                children = [(QTreeWidgetItem([f'{item.id} - {item.info["title"] if item.info != None else ""}']), item) for item in array if item.id in 
                            ' '.join(elem.id for elem in rt[1].children)]
                try:
                    rt[0].addChildren(list(zip(*children))[0])
                except Exception:
                    pass  
                for child in children:
                    add_children(child, [item for item in array if item not in list(zip(*children))[1]])

        main_roots = [(QTreeWidgetItem([f'{item.id} - {item.info["title"]}']), item)
                      for item in self.__tree if len(item.parents) == 0]

        for root in main_roots:
            add_children(root, [item for item in self.__tree if len(item.parents) != 0])

        [item[0].setChildIndicatorPolicy(QTreeWidgetItem.ShowIndicator) for item in main_roots
         if item[0].childCount() > 0]

        self.treeWidget.setColumnCount(1)
        self.treeWidget.addTopLevelItems([item for item, _ in main_roots])

        self.treeWidget.show()


def main():
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
