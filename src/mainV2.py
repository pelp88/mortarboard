import sys
from collections import deque

from PyQt5.QtCore import Qt, QObject, QThread, pyqtSignal
from PyQt5.QtGui import QIcon, QCursor
from PyQt5.QtWidgets import QApplication, QMainWindow, QErrorMessage

import ui.main

import lib.LibParseV2 as LibParse
import lib.LibExcel as LibExcel

class SearchWorker(QObject):
    finished = pyqtSignal()
    progress = pyqtSignal(int)

    def run(self):
        


class MainWindow(QMainWindow, ui.main.Ui_MainWindow):
    def __init__(self, parent=None):
        # ---- Window init ----
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.setWindowIcon(QIcon('img/icon.svg'))
        self.setWindowTitle('Mortarboard')
        
        # ---- Class initializations ----
        self.__parser = LibParse.Parser()
        self.__tree = LibParse.PatentTree()
        
        # ---- Misc ----
        self.spinBox.setValue(1)
        self.spinBox_2.setValue(1)
        self.treeView.setModel(self.__tree)

        # ---- Global vars ----
        self.__patent_windows = []

        # ---- Handlers ----
        self.pushButton.clicked.connect(self.do_search)
        self.pushButton_2.clicked.connect(self.do_excel)
            
    def do_error(self, error):
        QApplication.restoreOverrideCursor()
        error_dialog = QErrorMessage()
        error_dialog.setWindowTitle('Ошибка')
        if error == 'no_req':
            error_dialog.showMessage('Введите запрос!')
        elif error == 'no_patents_excel':
            error_dialog.showMessage('Отсутсвует информация о патентах, совершите поиск!')
        elif error == 'timeout':
            error_dialog.showMessage('Таймаут')
        self.__patent_windows.append(error_dialog)
        QApplication.restoreOverrideCursor()
        self.pushButton.setEnabled(True)

    def do_excel(self):
        if len(self.__tree) > 0:
            QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
            LibExcel.SaveContents(self.__tree, self.lineEdit.text())
            QApplication.restoreOverrideCursor()
        else:
            self.do_error(error='no_patents_excel')
    
    def do_search(self):
        self.progressBar.setValue(0)
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        request = self.lineEdit.text()
        pages = self.spinBox_2.value()
        quantity = self.spinBox.value()
        id_queue = deque()
        
        if len(request) == 0:
            self.do_error(error='no_req')
        else:
            if self.radioButton.isChecked():
                id_queue.extend(request.split(', '))
            elif self.radioButton_2.isChecked():
                for i in range(pages):
                    id_queue.extend(self.__parser.parse_search(request=request.split(), page_num=i))
            
            for i in range(quantity):
                current = id_queue.pop()
                patent = self.__tree.restore_patent(current)
                if patent is not None:
                    if 'citations' in patent.info:
                        id_queue.extendleft(patent.info['citations'])
                else:
                    info = self.__parser.parse_patent(patent_id=current)
                    if 'citations' in info:
                        id_queue.extendleft(info['citations'])
                    patent = self.__tree.create_patent(patent_id=current, info=info)
                self.progressBar.setValue((i / quantity) * 90)
            nones = [item for item in self.__tree if item.info == None]
            for item in nones:
                info = self.__parser.parse_patent(patent_id=item.id)
                item.set_params(info)
                self.progressBar.setValue(((nones.index(item) / len(nones)) * 10) + 90)
                
            self.__tree.cache_patents()
            self.__tree.bake_model()
            self.progressBar.setValue(100)
            QApplication.restoreOverrideCursor()
            self.pushButton.setEnabled(True)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
