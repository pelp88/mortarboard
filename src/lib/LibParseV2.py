from PyQt5.QtGui import QColor, QFont, QStandardItem, QStandardItemModel
from requests_html import HTMLSession
from datetime import datetime
import json, os


USE_CACHE = 0
CACHE_PATH = './cache/'


class Patent(QStandardItem):
    def __init__(self, id=None):
        super().__init__()
        self.__id = id
        self.__parents = []
        self.__children = []
        self.__applications = []
        self.__encounters = []
        self.__info = None
        self.setText(f'{self.id} - None')

        self.setEditable(False)
        
    def __repr__(self):
        return repr(f'{self.__id} - {self.__info}')
    
    def __str__(self):
        return str(f'{self.__id} - {self.__info}')
    
    def set_params(self, info: dict):
        _ = info.pop('citations')
        self.__info = info
        self.setText(f'{self.id} - {info["title"]}')
    
    @property
    def id(self):
        return self.__id
    
    @property
    def parents(self):
        return self.__parents
    
    @property
    def children(self):
        return self.__children
    
    @property
    def applications(self):
        return self.__applications
    
    @property
    def info(self):
        return self.__info

    @property
    def encounters(self):
        return len(self.__encounters)

    def add_encounter(self, id):
        self.__encounters.append(id)
        
    def add_parent(self, parent):
        self.__parents.append(parent)
    
    def add_child(self, child):
        self.__children.append(child)
        self.appendRow(child)
    
    def add_application(self, app):
        self.__applications.append(app)
        
    def to_cache(self):
        if USE_CACHE == 1:
            with open(f'{CACHE_PATH}{self.__id}.json', 'w') as file:
                file.write(json.dumps(self.__info))
                file.close()
        
    def from_cache(self):
        if USE_CACHE == 1:
            with open(f'{CACHE_PATH}{self.__id}.json', 'r') as file:
                self.__info = json.loads(file.read())
                file.close()
            
    def check_if_cached(self):
        if USE_CACHE == 1:
            listing = os.listdir(f'{CACHE_PATH}')
            if self.__id in ' '.join(listing):
                return True
            return False
                

class PatentTree(QStandardItemModel):
    def __init__(self):
        super().__init__()
        self.__patents = []
        self.__rootnode = self.invisibleRootItem()
        
    def __repr__(self):
        return '\n'.join([f'#{i}: {self.__patents[i - 1]}\n' for i in range(1, len(self.__patents) + 1)])
    
    def __iter__(self):
        return iter(self.__patents)
    
    def __len__(self):
        return len(self.__patents)

    def __getitem__(self, item):
        return self.__patents[item]

    def get_frequent(self, quantity):
        frequent = sorted(self.__patents, key=lambda x: len(x.children), reverse=True)
        if len(frequent) > quantity:
            return frequent[:quantity]
        else:
            return frequent

    def reset_model(self):
        self.clear()
        self.__patents = []
    
    def bake_model(self):
        for item in self.__patents:
            if len(item.parents) == 0:
                self.__rootnode.appendRow(item)
    
    def remove_patent(self, patent):
        print(f'[{datetime.now()}] Removed patent {patent.id}:\n{patent.info}')
        _ = self.__patents.pop(self.__patents.index(patent))
                        
    def get_patent(self, patent_id):
        result = [item for item in self.__patents if item.id == patent_id]
        return result[0] if len(result) > 0 else None
    
    def create_patent(self, patent_id: str, info: dict = None):
        if self.get_patent(patent_id) is None:
            patent = Patent(id=patent_id)
            self.__patents.append(patent)
            if info is not None:
                citations = info['citations']
                for item in citations:
                    if self.get_patent(item) is not None:
                        if patent_id not in self.get_patent(item).parents:
                            self.get_patent(item).add_parent(patent)
                        patent.add_child(self.get_patent(item))
                    else:
                        temp = self.create_patent(item)
                        temp.add_parent(patent)
                        patent.add_child(temp)
                patent.set_params(info)
        else:
            patent = self.get_patent(patent_id)
            patent.add_encounter(patent_id)
            if info is not None:
                if len(patent.children) == 0:
                    citations = info['citations']
                    for item in citations:
                        if self.get_patent(item) is not None:
                            if patent_id not in self.get_patent(item).parents:
                                self.get_patent(item).add_parent(patent)
                            patent.add_child(self.get_patent(item))
                        else:
                            temp = self.create_patent(item)
                            temp.add_parent(patent)
                            patent.add_child(temp)
                if patent.info is None:
                    patent.set_params(info)
            
        print(f'[{datetime.now()}] Created patent {patent.id}:\n{patent.info}')
        return patent
    
    def cache_patents(self):
        if USE_CACHE == 1:
            if not os.path.isfile(f'{CACHE_PATH}connections.json'):
                open(f'{CACHE_PATH}connections.json', 'w').close()
            with open(f'{CACHE_PATH}connections.json', 'w') as file:
                temp = {}
                for item in self.__patents:
                    if item.info is not None:
                        temp.update({item.id: [item.id for item in item.children]})
                        item.to_cache()
                file.write(json.dumps(temp))
                file.close()
                print(f'[{datetime.now()}] Cached all patents')
                
    def restore_patent(self, patent_id=None):
        if USE_CACHE == 1:
            if os.path.isfile(f'{CACHE_PATH}{patent_id}.json'):
                patent = None
                with open(f'{CACHE_PATH}connections.json', 'r') as file:
                    temp = json.loads(file.read())
                    for key, value in temp.items():
                        if key == patent_id:
                            patent = self.create_patent(patent_id=key, info={'citations': value})
                            patent.from_cache()
                    file.close()
                print(f'[{datetime.now()}] Restored cached patent {patent_id}')
                return patent
            else:
                return None   
            
class Parser:
    def __init__(self):
        self.__session = HTMLSession()
    
    def getter(self, url=None, sleep=0):
        html = self.__session.get(url)
        if 'page' in url:
            html.html.render(sleep=sleep)
        else:
            html.html.render()
        return html
    
    def parse_search(self, request=None, page_num=None):
        url = f'https://patents.google.com/?q={"+".join(request)}&oq={"+".join(request)}&page={page_num}'
        print(f'[{datetime.now()}] Getting search page on URL {url}')
        ids = []
        tags = self.getter(url=url, sleep=2).html.find('search-result-item') 
        for tag in tags:
            ids.append(tag.find('state-modifier')[0].attrs['data-result'][7:-3])
        return ids
    
    def parse_patent(self, patent_id=None):
        url = f'https://patents.google.com/patent/{patent_id}'
        print(f'[{datetime.now()}] Getting result page on URL {url}')
        root = self.getter(url=url).html.find('patent-result')[0]
        
        inventor_name = [item.attrs['data-inventor'] for item in root.find('[data-inventor]')]
        assignee = [item.attrs['data-assignee'] for item in root.find('[data-assignee]')]
        title = [item.text for item in root.find('h1') if 'id' in item.attrs][0][28:]
        country = [item.text for item in root.find('[class="tagline style-scope patent-result"]')][0]
        
        try:
            pdf_url = [item.attrs['href'] for item in root.find('[target="_blank"]')][0]
        except:
            pdf_url = ''
        
        try:
            applications = {item.find('[class="year style-scope application-timeline"]')[0].text: 
                [[item.find('[id="cc"]')[i].text, item.find('[data-result]')[i].attrs['data-result'][7:-3]] for i in range(len(item.find('[id="cc"]')))] 
                for item in root.find('[class="block style-scope application-timeline"]')}
        except:
            applications = []
        
        try:
            citations = [item.attrs['data-result'][7:-3] for item in root.find('[id="patentCitations"]~[class="responsive-table style-scope patent-result"]')[0].find('state-modifier')]
        except:
            citations = []
        
        return {'inventor': inventor_name,
                'assignee': assignee,
                'title': title,
                'applications': applications,
                'country': country,
                'citations': citations,
                'pdf_url': pdf_url}
