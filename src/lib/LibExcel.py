import openpyxl, collections
import openpyxl.chart as charts
from datetime import datetime

from openpyxl.styles import Border, Side, Font


class SaveContents:
    def __init__(self, tree=None, request='None'):
        self.__tree = tree
        self.__filename = f'./{datetime.now()} --- {request}.xlsx'
        print(f'[{datetime.now()}] Excel started')
        self.make_file()

    def counter(self, patent):
        value = 0

        for item in self.__tree:
            if patent.id == item.id:
                value += 1

        return value

    def make_file(self):
        def as_text(value):
            if value is None:
                return ""
            return str(value)

        file = openpyxl.Workbook()

        thin_border = Border(left=Side(style='thin'),
                                right=Side(style='thin'),
                                bottom=Side(style='thin'))

        thick_border = Border(left=Side(style='thick'),
                                right=Side(style='thick'),
                                bottom=Side(style='thick'),
                                top=Side(style='thick'))

        sheet1 = file.active

        sheet1.cell(row=1, column=1, value='ID')
        sheet1.cell(row=1, column=2, value='Название')
        sheet1.cell(row=1, column=3, value='Кол-во повторений')
        sheet1.cell(row=1, column=4, value='Фирма')
        sheet1.cell(row=1, column=5, value='Изобретатели')
        sheet1.cell(row=1, column=7, value='Страна')
        sheet1.cell(row=1, column=6, value='Применения')

        for item in sheet1.iter_rows(min_row=1, max_row=1, min_col=1, max_col=8):
            for tuple_item in item:
                tuple_item.border = thick_border
                tuple_item.font = Font(bold=True)

        counter = 2

        for item in self.__tree:
            sheet1.cell(row=counter, column=1, value=item.id)
            sheet1.cell(row=counter, column=2, value=item.info['title'])
            sheet1.cell(row=counter, column=3, value=item.encounters)
            sheet1.cell(row=counter, column=4, value=', '.join(item.info["assignee"]))
            sheet1.cell(row=counter, column=5, value=', '.join(item.info['inventor']))
            sheet1.cell(row=counter, column=6, value=f'{item.info["applications"]}')
            sheet1.cell(row=counter, column=7, value=f'{item.info["country"]}')

            counter += 1

        for item in sheet1.iter_rows(min_row=2, max_row=counter - 1, min_col=1, max_col=8):
            for tuple_item in item:
                tuple_item.border = thin_border

        ##################################

        for column_cells in sheet1.columns:
            length = max(len(as_text(cell.value)) for cell in column_cells)
            sheet1.column_dimensions[column_cells[0].column_letter].width = length + 2

        sheet2 = file.create_sheet(title='Таблицы')
        years = collections.Counter([list(item.info['applications'].keys())[0] for item in self.__tree])

        counter1 = 1
        for item in self.__tree:
            sheet2.append((item.id, item.encounters))
            counter1 += 1

        [sheet2.append([' ']) for _ in range(10)]

        counter2 = 1
        for key in years:
            try:
                sheet2.append((key, years[key]))
                counter2 += 1
            except:
                pass

        chart1 = charts.BarChart()
        chart1.title = 'По повторениям'
        chart1.legend = None
        chart1.y_axis.majorGridlines = None
        chart1.varyColors = True

        data1 = charts.Reference(sheet2, min_col=2, min_row=1, max_col=2, max_row=counter1)
        labels1 = charts.Reference(sheet2, min_col=1, min_row=1, max_row=counter1)
        chart1.add_data(data1, titles_from_data=True)
        chart1.set_categories(labels1)
        sheet2.add_chart(chart1, 'E2')

        ############################################

        chart2 = charts.BarChart()
        chart2.title = 'Кол-во по годам'
        chart2.legend = None
        chart2.y_axis.majorGridlines = None
        chart2.varyColors = True

        data2 = charts.Reference(sheet2, min_col=2, min_row=10 + counter1, max_col=2,
                                    max_row=9 + counter1 + counter2)
        labels2 = charts.Reference(sheet2, min_col=1, min_row=10 + counter1, max_row=9 + counter1 + counter2)
        chart2.add_data(data2, titles_from_data=True)
        chart2.set_categories(labels2)
        sheet2.add_chart(chart2, 'E18')

        file.save(filename=self.__filename)
        print('[{datetime.now()}] Excel finished!')


if __name__ == '__main__':
    pass
