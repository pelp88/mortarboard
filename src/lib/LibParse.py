from bs4 import BeautifulSoup
from requests_html import AsyncHTMLSession
from statistics import mean
from datetime import datetime
import time, json, asyncio


CACHE_PATH = './cache/'


class Patent:
    def __init__(self, id=None):
        self.__id = id
        self.__parents = []
        self.__children = []
        self.__applications = []
        self.__info = {'title': None,
                       'pdf_url': None,
                       'application_date': None,
                       'inventor_name': None,
                       'assignee_name': None,
                       'country': None}
    
    def set_params(self, title=None, pdf_url=None, app_date=None, inv_name=None, ass_name=None, country=None):
        if title is not None:
            self.__info['title'] = title
        if pdf_url is not None:
            self.__info['pdf_url'] = pdf_url
        if app_date is not None:
            self.__info['app_date'] = app_date
        if inv_name is not None:
            self.__info['inv_name'] = inv_name
        if ass_name is not None:
            self.__info['ass_name'] = ass_name
        if country is not None:
            self.__info['country'] = country
    
    @property
    def id(self):
        return self.__id
    
    @property
    def parents(self):
        return self.__parents
    
    @property
    def children(self):
        return self.__children
    
    @property
    def applications(self):
        return self.__applications
    
    @property
    def info(self):
        return self.__info
    
    def add_parent(self, parent):
        self.__parents.append(parent)
    
    def add_child(self, child):
        self.__children.append(child)
    
    def add_application(self, app):
        self.__applications.append(app)
        
    def to_cache(self):
        with open(f'{CACHE_PATH}{self.__id}.json', 'w') as file:
            file.write(json.dumps(self.__info))
            file.close()
        
    def from_cache(self):
        with open(f'{CACHE_PATH}{self.__id}.json', 'r') as file:
            self.__info = json.loads(file.read())
            file.close()


class PatentsParserNew:
    def __init__(self, request=None, pages=1, quantity=10):
        self.__event_loop = asyncio.get_event_loop()
        self.__session = AsyncHTMLSession()
        self.__request = request.split()
        self.__pages = pages
        self.__quantity = quantity
        self.__results = []
        
    def get_pages(self, url=None):
        """
        Async page getter
        Args:
            urls ([list]): [list of IRLs]
        """
        async def get_url(self, url):
            html = await self.__session.get(url)
            await html.html.arender()
            return html.html.html
        
        return self.__event_loop.run_until_complete(get_url(self, url))

    def get_search_pages(self):
        """
        Get and parse search pages with given request and given quantity (patents.google.com)
        Returns:
            [list]: [list of patent IDs]
        """
        urls = [f'https://patents.google.com/?q={"+".join(self.__request)}&oq={"+".join(self.__request)}&page={i}' for i in range(self.__pages)]
        pages = self.get_pages(urls=urls)
        to_parse = []
        for page in pages:
            soup = BeautifulSoup(page, features="lxml")
            for item in soup.find_all('state-modifier', attrs={"class": "result-title style-scope search-result-item"}):
                result = item['data-result']
                if result.find('patent') != -1:
                    to_parse.append(result[7:-3])
                    
        return to_parse

    def parse_patent_page(self, patent_id):
        """
        Get and parse patent from patent.google.com
        Args:
            patent_id ([string]): [ID of patent]
        """
        url = f'https://patents.google.com/patent/{patent_id}'
        print(f'[{datetime.now()}] Getting result page on URL {url}')
        webpage = self.get_pages(url=url)
        soup = BeautifulSoup(webpage, features="lxml")

        try:
            inventor_name = [data.get_text() for data in soup.find_all('dd', itemprop='inventor')]
        except Exception:
            inventor_name = []

        try:
            assignee_name = [data.get_text() for data in soup.find_all('dd', itemprop='assigneeCurrent')]
            if len(assignee_name) == 0:
                assignee_name = [data.get_text() for data in soup.find_all('dd', itemprop='assigneeOriginal')]
        except Exception:
            assignee_name = []

        title = soup.find('title').get_text().replace('- Google Patents', '').replace(f'{patent_id} - ', '').strip()
        applications = [data.get_text()[7:-3] for data in soup.find_all('span', itemprop="documentId") if
                        patent_id != data.get_text()[7:-3]]
        #application_date = soup.find('span', itemprop='year').get_text()
        #country = soup.find('dd', itemprop='countryName').get_text()

        citations = []

        found_cites_orig = soup.find_all('tr', itemprop="backwardReferencesOrig")
        if len(found_cites_orig) > 0:
            for citation in found_cites_orig:
                citations.append(citation.find('span', itemprop='publicationNumber').get_text())

        found_cites_family = soup.find_all('tr', itemprop="backwardReferencesFamily")
        if len(found_cites_family) > 0:
            for citation in found_cites_family:
                citations.append(citation.find('span', itemprop='publicationNumber').get_text())

        pdf_url = ''.join(soup.find('a').get_attribute_list('href'))
        if pdf_url[0] != 'h':
            pdf_url = 'Отсутствует'
            
        print(f'[{datetime.now()}] Patent {patent_id} parsed')
        
        return {'ID': patent_id,
                'applications': applications,
                'parents': [],
                'children': citations,
                'info': {'title': title,
                            'pdf_url': pdf_url,
                            #'application_date': application_date,
                            'inventor_name': inventor_name,
                            'assignee_name': assignee_name,
                            #'country': country
                            }}


class PatentsParser:
    def __init__(self, request=None, pages=1, depth=0):
        self.__request = request.split()
        self.__pages = pages
        self.__depth = depth
        self.__parsed = []
        for item in self.get_search_pages():
            self.get_data(item, self.__depth)
        self.make_parents()

    def __iter__(self):
        return iter(self.__parsed)

    def get_search_pages(self):
        chrome_opts = webdriver.ChromeOptions()
        chrome_opts.headless = True
        driver = webdriver.Chrome(executable_path=r'./chromedriver.exe', chrome_options=chrome_opts)
        to_parse = []

        for i in range(self.__pages):
            try:
                driver.get(
                    f'https://patents.google.com/?q={"+".join(self.__request)}&oq={"+".join(self.__request)}&page={i}'
                )
                print(
                    f'https://patents.google.com/?q={"+".join(self.__request)}&oq={"+".join(self.__request)}&page={i}')
                time.sleep(5)
                elements = [item.get_attribute('data-result') for item in
                            driver.find_elements_by_tag_name('state-modifier')]
                [to_parse.append(item[7:-3]) for item in elements if str(item).find('patent') != -1]
            except:
                driver.close()

        return to_parse

    def parse_patent_page(self, patent_id):
        """
        Brew and parse BS4 soup from patent page of patents.google.com
        :param patent_id: ID of patent
        :return: dict of parsed results
        """
        print(patent_id)
        url = f'https://patents.google.com/patent/{patent_id}'
        webpage = urlopen(Request(url, headers={'User-Agent': 'Mozilla/5.0'})).read()
        soup = BeautifulSoup(webpage, features="lxml")

        try:
            inventor_name = [data.get_text() for data in soup.find_all('dd', itemprop='inventor')]
        except Exception:
            inventor_name = []

        try:
            assignee_name = [data.get_text() for data in soup.find_all('dd', itemprop='assigneeCurrent')]
            if len(assignee_name) == 0:
                assignee_name = [data.get_text() for data in soup.find_all('dd', itemprop='assigneeOriginal')]
        except Exception:
            assignee_name = []

        title = soup.find('title').get_text().replace('- Google Patents', '').replace(f'{patent_id} - ', '').strip()
        applications = [data.get_text()[7:-3] for data in soup.find_all('span', itemprop="documentId") if
                        patent_id != data.get_text()[7:-3]]
        application_date = soup.find('span', itemprop='year').get_text()
        country = soup.find('dd', itemprop='countryName').get_text()

        citations = []

        found_cites_orig = soup.find_all('tr', itemprop="backwardReferencesOrig")
        if len(found_cites_orig) > 0:
            for citation in found_cites_orig:
                citations.append(citation.find('span', itemprop='publicationNumber').get_text())

        found_cites_family = soup.find_all('tr', itemprop="backwardReferencesFamily")
        if len(found_cites_family) > 0:
            for citation in found_cites_family:
                citations.append(citation.find('span', itemprop='publicationNumber').get_text())

        pdf_url = ''.join(soup.find('a').get_attribute_list('href'))
        if pdf_url[0] != 'h':
            pdf_url = 'Отсутствует'

        return {'ID': patent_id,
                'applications': applications,
                'parents': [],
                'children': citations,
                'info': {'title': title,
                         'pdf_url': pdf_url,
                         'application_date': application_date,
                         'inventor_name': inventor_name,
                         'assignee_name': assignee_name,
                         'country': country}}

    def get_data(self, n_pat, lvl):
        """
        Recursively make dict of patents' citations with given level of recursion
        :param n_pat: ID of upper-most patent
        :param lvl: level of recursion
        :return: dict of patents' citations
        """
        print(f'Уровень: {lvl}')
        parsed = self.parse_patent_page(n_pat)

        if lvl > 0:
            for item in parsed['children']:
                self.get_data(item, lvl - 1)

        self.__parsed.append(parsed)

    def make_parents(self):
        """
        Finding and setting parent patent
        """
        print('\n\nПодготовка данных...\n')
        for item_1 in self.__parsed:
            for item_2 in self.__parsed:
                if str(item_2['ID']) in ' '.join(item_1['children']):
                    item_2['parents'].append(item_1['ID'])


if __name__ == '__main__':
    a = PatentsParserNew('кориолисов расходомер', 4, 50)
    avg = []
    for _ in range(10):
        start = time.time()
        b = a.parse_patent_page('RU2305257C2')
        end = time.time() - start
        avg.append(end)
        print(b)
    print(mean(avg))